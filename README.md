# template uploads spring
## Proyecto plantilla para subida y descarga de archivos en spring
Proyecto maqueta o plantilla para generar un rest api para subida y descarga de archivos
relizada con maven, a las que se les hicieron ciertas modificaciones especificadas en
el changelog.

Referencia del Ejemplo
Fuente Web: https://www.bezkoder.com/spring-boot-file-upload/
Repositorio: https://github.com/bezkoder/spring-boot-upload-multipart-files

## Futuras mejoras
- pruebas unitarias

## Tecnologias aplicadas
- java 1.8
- springboot v2.7.3
- gradle-6.8