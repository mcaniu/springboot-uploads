package com.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class AppConstant {

    public static String ERROR_NOT_INIT_FOLDER = "No se pudo inicializar la carpeta para cargar!";
    public static String ERROR_NOT_READ_FILE = "No se pudo leer el archivo!";
    public static String ERROR_NOT_READ_FILES = "No se pudieron cargar los archivos!";
    public static String ERROR_FILE_TOO_LARGE = "El tamaño del archivo excede el limite";
    public static String ERROR_FILE_EXIST = "Ya existe un archivo con ese nombre";
    public static String UPLOAD_SUCCESS = "Subida de archivo exitosa: ";


    // metodo que ocupa reflexion
    public static Map<String, Object> convertirDTOaHashMap(Object objetoDTO) {
        Map<String, Object> mapa = new HashMap<>();

        Class<?> claseDTO = objetoDTO.getClass();
        Field[] campos = claseDTO.getDeclaredFields();

        for (Field campo : campos) {
            campo.setAccessible(true);
            try {
                Object valor = campo.get(objetoDTO);
                mapa.put(campo.getName(), valor);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return mapa;
    }


}
