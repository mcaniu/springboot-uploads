package com.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileExeption extends Exception {

    private String message;

    public FileExeption(String message) {
        super();
        this.message = message;
    }
}
