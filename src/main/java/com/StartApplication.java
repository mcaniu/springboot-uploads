package com;

import com.services.FileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class StartApplication implements CommandLineRunner {

    @Resource
    FileService storageService;

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

    /**
     * Se eliminan y se crean las carpetas de subida y descarga de archivos
     */
    @Override
    public void run(String... arg) {
        storageService.eliminar();
        storageService.inicializarCarpetaFile();
    }
}
