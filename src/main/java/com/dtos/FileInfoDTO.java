package com.dtos;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**
 * DTO que transportaran los datos de informacion del archivo.
 */
@Getter
@Setter
@Builder
public class FileInfoDTO {

    private String name;
    private String url;


}
