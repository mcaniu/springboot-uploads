package com.services;

import com.exceptions.FileExeption;
import com.utils.AppConstant;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static com.utils.AppConstant.ERROR_NOT_INIT_FOLDER;

@Service
public class FileServiceImpl implements FileService {

    private final Path root = Paths.get("uploads");

    @Override
    public void inicializarCarpetaFile() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException(ERROR_NOT_INIT_FOLDER);
        }
    }

    @Override
    public void subir(MultipartFile file) throws FileExeption {
        try {
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
        } catch (Exception e) {
            if (e instanceof FileAlreadyExistsException) {
                throw new FileExeption(AppConstant.ERROR_FILE_EXIST);
            }
            if (e instanceof MaxUploadSizeExceededException) {
                throw new FileExeption(AppConstant.ERROR_FILE_TOO_LARGE);
            }

            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Resource descargar(String filename) throws FileExeption {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new FileExeption(AppConstant.ERROR_NOT_READ_FILE);
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }

    }

    @Override
    public void eliminar() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> descargar() throws FileExeption {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new FileExeption(AppConstant.ERROR_NOT_READ_FILES);
        }
    }

}
