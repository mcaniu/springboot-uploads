package com.services;

import com.exceptions.FileExeption;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * Servicio de manipulacion de los archivos.
 */

public interface FileService {

    /**
     * Metodo que crea la carpeta en donde se almacenaran los archivos cargados
     */
    void inicializarCarpetaFile();

    /**
     * Metodo que carga los archivos a una carpeta del sistema
     *
     * @param file de subida.
     */
    void subir(MultipartFile file) throws IOException, FileExeption;


    /**
     * Metodo que obtiene el archivo buscado
     *
     * @param filename nombre del archivo buscado.
     * @return devuelve un Resource del archivo asociado.
     */
    Resource descargar(String filename) throws FileExeption;

    /**
     * Metodo que obtiene todos los archivos cargados en el sistema
     *
     * @return devuelve un Stream de todos los archivos cargados en el sistema.
     */
    Stream<Path> descargar() throws IOException, FileExeption;


    /**
     * Metodo que elimina todos los archivos en el sistema
     */
    void eliminar();


}
