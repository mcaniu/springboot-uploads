package com.controllers;

import com.dtos.FileInfoDTO;
import com.dtos.ResponseDTO;
import com.exceptions.FileExeption;
import com.services.FileService;
import com.utils.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.utils.AppConstant.UPLOAD_SUCCESS;


/**
 * Controlador para manejar la subida y bajada de archivos
 */

@RestController
@RequestMapping("/v1/files")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * Servicio para descargar todos los files en el almacenamiento
     *
     * @return devuelve un ResponseEntity con la informacion de cada unos de los Files.
     */
    @GetMapping
    public ResponseEntity<?> getListFiles() throws IOException, FileExeption {
        List<FileInfoDTO> fileInfoDTOS = fileService.descargar().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FileController.class, "getFile", path.getFileName().toString()).build().toString();

            return FileInfoDTO.builder().name(filename).url(url).build();
        }).collect(Collectors.toList());

        ResponseDTO response = ResponseDTO.builder()
                .timestamp(new Date())
                .message("")
                .details("OK")
                .build();

        Map<String, Object> json = AppConstant.convertirDTOaHashMap(response);
        json.put("archivos", fileInfoDTOS);

        return ResponseEntity.status(HttpStatus.OK).body(json);
    }

    /**
     * Servicio para subir un archivo
     *
     * @param file de subida.(NOTA: si se envian varios archivos, solo tomara el primero)
     * @return devuelve un ResponseEntity con un mensaje de exito o un mensaje de error si falla.
     */
    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IOException, FileExeption {
        fileService.subir(file);
        ResponseDTO response = ResponseDTO.builder()
                .timestamp(new Date())
                .message(UPLOAD_SUCCESS + file.getOriginalFilename())
                .details("OK")
                .build();

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    /**
     * Servicio para descargar un archivo
     *
     * @param filename del archivo asociado.
     * @return devuelve un ResponseEntity con el archivo encontrado.
     */
    @GetMapping("/{filename:.+}")
    public ResponseEntity<?> getFile(@PathVariable String filename) throws FileExeption {
        Resource file = fileService.descargar(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
